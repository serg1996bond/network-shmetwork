package com.example.network.subdomains.posting.domain.model.aggregates;

import com.example.network.subdomains.posting.domain.model.valueobjects.AuthorId;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.util.UUID;

@Entity
@NoArgsConstructor
public class Author {

    @EmbeddedId
    private AuthorId authorId;

    public Author(UUID userId) {

    }
}
