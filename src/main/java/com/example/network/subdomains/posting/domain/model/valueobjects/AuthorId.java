package com.example.network.subdomains.posting.domain.model.valueobjects;

import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
public class AuthorId implements Serializable {
    @Id
    private UUID id;
}
