package com.example.network.subdomains.identification.domain.model.services;

import com.example.network.subdomains.identification.domain.model.events.IdentificationEvent;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class IdentificationDomainEventPublisher {

    private final ApplicationEventPublisher applicationEventPublisher;

    public <T extends IdentificationEvent> void publish(final T identificationEvent) {
        applicationEventPublisher.publishEvent(identificationEvent);
    }

}
