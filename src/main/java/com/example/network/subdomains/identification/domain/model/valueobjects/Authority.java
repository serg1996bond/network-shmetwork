package com.example.network.subdomains.identification.domain.model.valueobjects;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Authority implements GrantedAuthority {
    private String Authority;
}
