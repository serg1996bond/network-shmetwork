package com.example.network.subdomains.identification.domain.model.aggregates;

import com.example.network.subdomains.identification.domain.model.valueobjects.Authority;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

/**
 * класс-представление пользователя в системе
 * используется для авторизации/аутентификации
 */

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "account")
@NoArgsConstructor
@Getter
public class User implements UserDetails {

    @Id
    private UUID id;

    private LocalDateTime removedDate;

    /**логин пользователя*/
    @Column(name = "login")
    private String username;
    /**захешированный пароль пользователя*/
    @Column(name = "password_hash")
    private String password;

    /**Список ролей для авторизации*/
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "account_authorities", joinColumns = @JoinColumn(name = "user_id"))
    private Set<Authority> authorities;

    public User(UUID id, String username, String password, Set<Authority> authorities) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}