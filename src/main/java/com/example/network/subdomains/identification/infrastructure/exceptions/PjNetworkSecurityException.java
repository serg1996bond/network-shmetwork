package com.example.network.subdomains.identification.infrastructure.exceptions;

class PjNetworkSecurityException extends RuntimeException{
    public PjNetworkSecurityException() {
        super();
    }

    public PjNetworkSecurityException(String message) {
        super(message);
    }
}
