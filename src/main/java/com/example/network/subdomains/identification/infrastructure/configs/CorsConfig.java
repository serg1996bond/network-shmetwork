package com.example.network.subdomains.identification.infrastructure.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfig implements WebMvcConfigurer {
    /**
     * Настройка CORS
     * @param registry - параметры CORS
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //пока нет необходимости в другом - корс настроен помечать все запросы
        registry.addMapping("/**")
                .allowedMethods("*")
                .allowedOrigins("*")
                .allowedHeaders("*");
    }
}
