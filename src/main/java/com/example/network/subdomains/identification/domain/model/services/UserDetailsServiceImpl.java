package com.example.network.subdomains.identification.domain.model.services;

import com.example.network.subdomains.identification.domain.model.repositories.UserRepository;
import com.example.network.subdomains.identification.infrastructure.exceptions.UserNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsernameAndRemovedDateIsNull(username)
                .orElseThrow(UserNotFoundException::new);
    }
}
