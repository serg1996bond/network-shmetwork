package com.example.network.subdomains.identification.interfaces.rest.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RegisterUserModel {
    private final String login;
    private final String password;
}
