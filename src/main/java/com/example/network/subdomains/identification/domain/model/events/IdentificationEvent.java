package com.example.network.subdomains.identification.domain.model.events;

import java.time.LocalDateTime;

public interface IdentificationEvent {
    LocalDateTime getEventTime();
}
