package com.example.network.subdomains.identification.application.services;

import com.example.network.subdomains.identification.application.dto.RegisterUserDto;
import com.example.network.subdomains.identification.domain.model.aggregates.User;
import com.example.network.subdomains.identification.domain.model.repositories.UserRepository;
import com.example.network.subdomains.identification.domain.model.valueobjects.Authority;
import com.example.network.subdomains.identification.infrastructure.exceptions.LoginAlreadyTakenException;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.UUID;

@Service
@AllArgsConstructor
public class RegistrationService {

    private final PasswordEncoder encoder;
    private final UserRepository userRepository;

    public void registerUser(RegisterUserDto dto){
        var login = dto.getLogin();
        if (userRepository.existsByUsername(login)) {
            throw new LoginAlreadyTakenException();
        }
        var userId = UUID.randomUUID();
        var password = encoder.encode(dto.getPassword());
        var newUser = new User(userId, login, password, Set.of(new Authority("USER_ROLE")));
        userRepository.save(newUser);
    }
}
