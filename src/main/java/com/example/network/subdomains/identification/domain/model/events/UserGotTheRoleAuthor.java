package com.example.network.subdomains.identification.domain.model.events;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@AllArgsConstructor
public class UserGotTheRoleAuthor implements IdentificationEvent{
    private final UUID userId;
    private final LocalDateTime eventTime;
}
