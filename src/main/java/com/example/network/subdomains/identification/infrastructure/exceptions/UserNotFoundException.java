package com.example.network.subdomains.identification.infrastructure.exceptions;

public class UserNotFoundException extends PjNetworkSecurityException {
    public UserNotFoundException() {
        super();
    }

    public UserNotFoundException(String message) {
        super(message);
    }
}
