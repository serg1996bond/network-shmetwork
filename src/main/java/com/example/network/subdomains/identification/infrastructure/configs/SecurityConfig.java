package com.example.network.subdomains.identification.infrastructure.configs;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Класс, наследуется от @see WebSecurityConfigurerAdapter
 * представляет собой настройку авторизации/аутентификации
 * на данным момент настроен на Basic Auth
 */

@Configuration
@AllArgsConstructor
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService detailsService;

    /**
     * В данном методе переопределяется какой UserDetailService будет использоваться
     * и сообщается какой Encoder используется для хеширования паролей
     * @param auth билдер менеджера аутентификации
     * @throws Exception по требованию фреймворка Spring Security
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(detailsService).passwordEncoder(passwordEncoder());
    }

    /**
     * в данном методе производится настройка безопасности web-приложения
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                    .cors()
                .and()
                    .httpBasic()
                .and()
                    .csrf()
                    .disable();
    }

    /**
     * Класс, имплементация @see UserDetailsService
     * содержит логику по превращению @see User, найденного по логину, в @see UserDetails
     * для последующей передачи механизму аутентификации/авторизации
     */



    //создает бин класса хеширующиего пароли
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
