package com.example.network.subdomains.identification.domain.model.repositories;

import com.example.network.subdomains.identification.domain.model.aggregates.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends CrudRepository<User, UUID> {
    Optional<User> findByUsernameAndRemovedDateIsNull(String username);

    boolean existsByIdAndRemovedDateIsNull(UUID userId);
    boolean existsByUsername(String username);
}
