package com.example.network.subdomains.identification.interfaces.rest.controllers;

import com.example.network.subdomains.identification.application.dto.RegisterUserDto;
import com.example.network.subdomains.identification.application.services.RegistrationService;
import com.example.network.subdomains.identification.interfaces.rest.models.RegisterUserModel;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/registration")
@AllArgsConstructor
public class RegistrationController {

    private final RegistrationService registrationService;
    private final ModelMapper mapper;

    @PostMapping("user")
    public void registerUser(@RequestBody RegisterUserModel model){
        var registerUserDto = mapper.map(model, RegisterUserDto.class);
        registrationService.registerUser(registerUserDto);
    }

}
